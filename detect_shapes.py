


from formas980.shapedetector import ShapeDetector
import argparse
import imutils
import cv2
import numpy as np


image = cv2.imread('foto.png')

resized = imutils.resize(image, width=300)
ratio = image.shape[0] / float(resized.shape[0])


gray = cv2.cvtColor(resized, cv2.COLOR_BGR2GRAY)
color = cv2.cvtColor(resized, cv2.COLOR_BGR2HSV)

blurred_frame = cv2.GaussianBlur(gray, (11,11), 0)
edged3 = cv2.Canny(blurred_frame, 10, 30)
#hsv = cv2.cvtColor(blurred_frame, cv2.COLOR_BGR2HSV)





blurred = cv2.GaussianBlur(gray, (5, 5), 0)

edged = cv2.Canny(blurred, 50, 100, 200)
edged2 = cv2.Canny(blurred, 0, 10, 255)
thresh = cv2.threshold(blurred, 60, 255, cv2.THRESH_BINARY)[1]
#ddd=cv2.cvtColor(thresh,cv2.COLOR_GRAY2BGR)
# find contours in the thresholded image and initialize the
# shape detector
cnts = cv2.findContours(thresh.copy(), cv2.RETR_EXTERNAL,
	cv2.CHAIN_APPROX_SIMPLE)
cnts = imutils.grab_contours(cnts)
sd = ShapeDetector()


(contornos,_) = cv2.findContours(edged.copy(), cv2.RETR_EXTERNAL, cv2.CHAIN_APPROX_SIMPLE)



for c in cnts:
	
	M = cv2.moments(c)
	cX = int((M["m10"] / M["m00"]) * ratio)
	cY = int((M["m01"] / M["m00"]) * ratio)
	shape = sd.detect(c)

	
	c = c.astype("float")
	c *= ratio
	c = c.astype("int")
	cv2.drawContours(image, [c], -1, (0, 255, 0), 2)
	text=cv2.putText(image, shape, (cX, cY), cv2.FONT_HERSHEY_SIMPLEX,
		0.5, (255, 0, 255), 2)

	# show the output image
	cv2.imshow("Image", text)
	
	color=cv2.imshow("contorno", edged)


      
pil=cv2.drawContours(edged,contornos,-1,(110,200,90),4)
pil2 = cv2.cvtColor(pil, cv2.COLOR_GRAY2BGR)
pil2=cv2.drawContours(pil2,contornos,-1,(150,86,210),4)
cv2.imshow("contorno pintado",pil2)
cv2.waitKey(0)
